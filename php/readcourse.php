<?php
include("../function/connections.php");
include("../function/StringTrimmer.php");
if (isset($_GET["postid"]))
    $postid = $_GET["postid"];
$postquery = "SELECT * FROM `course_table` WHERE `course_id` =$postid;";
$postresult = mysqli_query($conn, $postquery);
$postdetail = mysqli_fetch_assoc($postresult);
/*$postquery = "SELECT * FROM `course_table` WHERE `course_id` = ?;";
$postresult = $connect->prepare($postquery);
$postresult -> bindValue(1,$postid);
$postresult->execute();
$postdetail=$postresult->fetchColumn();*/



include "header.php";
?>
<div class="container-lg" align="center">
    <h1 align="center"><?php echo $postdetail["course_title"] ?></h1>
    <hr>
    <div class="col">
        <div class="col-lg course-section">
            <p style="font-size: 20px; text-align: left "><?php echo $postdetail["course_data"] ?></p>
            <img src="data:image/png;base64, <?php echo base64_encode($postdetail["course_image"]) ?>" width="25%" height="25%" alt="IMAGE COURSE">
            <hr>

        </div>

    </div>
    <div class="col-lg">
        <a href=<?php echo "exam.php?courseid=$postdetail[course_id]" ?> class="btn btn-danger"><h3> Start Exam </h3></a>
    </div>
</div>

